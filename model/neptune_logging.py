


def log_loss_training(opt,loss,step):
    for i in loss.keys():
        opt.neptune.log_metric(i,loss[i]/step)
