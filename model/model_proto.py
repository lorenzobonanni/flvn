import torch.nn as nn
import torch
import torchvision.models as models
import torch.nn.functional as F
import ltn
from ltn.Predicate_ltn import Has_attribute


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Linear') != -1:
        m.weight.data.normal_(0.0, 0.02)
        m.bias.data.fill_(0)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)


class LINEAR_SOFTMAX_ALE(nn.Module):
    def __init__(self, input_dim, attri_dim):
        super(LINEAR_SOFTMAX_ALE, self).__init__()
        self.fc = nn.Linear(input_dim, attri_dim)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x, attribute):
        middle = self.fc(x)
        output = self.softmax(middle.mm(attribute))
        return output


class LINEAR_SOFTMAX(nn.Module):
    def __init__(self, input_dim, output_dim):
        super(LINEAR_SOFTMAX, self).__init__()
        self.fc = nn.Linear(input_dim, output_dim)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        x = self.fc(x)
        x = self.softmax(x)
        return x


class LAYER_ALE(nn.Module):
    def __init__(self, input_dim, attri_dim):
        super(LAYER_ALE, self).__init__()
        self.fc = nn.Linear(input_dim, attri_dim)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x, attribute):
        batch_size = x.size(0)
        x = torch.mean(x, dim=1)
        x = x.view(batch_size, -1)
        middle = self.fc(x)
        output = self.softmax(middle.mm(attribute))
        return output


def same_attribute(x, y):
    return torch.gather(torch.gt(y, 0), 1, x)


def not_same_attribute(x, y):
    return torch.gather(torch.le(y, 0), 1, x)


def cosine_similarity(x, y, c):
    dot_product = torch.sum(torch.multiply(x, y), dim=1)
    x = torch.sqrt(torch.sum(torch.square(x), dim=1))
    y = torch.sqrt(torch.sum(torch.square(y), dim=1))
    similarity = dot_product / (x * y)
    return 1 / (1 + torch.exp(-c * similarity))


def same_attribute_error(x, y):
    return torch.gather(torch.gt(y, 0), 1, x)


def not_same_attribute_error(x, y):
    return torch.gather(torch.le(y, 0), 1, x)


def get_and_of_attributes(x, y):
    # x[torch.logical_not(torch.gt(y, 0))] = 1
    # x[torch.gt(y, 0))] = 1
    x = torch.where(torch.gt(y, 0), x, 1 - x)
    return torch.min(x, dim=1)[0]


def get_and_of_attributes_error(x, y):
    # x[torch.logical_not(torch.gt(y, 0))] = 1
    # x[torch.gt(y, 0))] = 1
    mask = torch.gt(y, 0)
    false_indices = torch.randint(low=0, high=x.shape[1], size=(x.shape[0], 15))
    mask[torch.arange(y.shape[0]).unsqueeze(1).type(torch.LongTensor), false_indices] = False
    x = torch.where(torch.gt(y, 0), x, 1 - x)
    return torch.min(x, dim=1)[0]


class resnet_proto_IoU(nn.Module):
    def __init__(self, opt, attribute):
        super(resnet_proto_IoU, self).__init__()
        resnet = models.resnet101(pretrained=True)
        num_ftrs = resnet.fc.in_features
        num_fc_dic = {'cub': 150, 'awa2': 40, 'sun': 645}
        self.extract = ['layer4']  # 'layer1', 'layer2', 'layer3', 'layer4'
        self.dim_dict = {'layer1': 56 * 56, 'layer2': 28 * 28, 'layer3': 14 * 14, 'layer4': 7 * 7, 'avg_pool': 1 * 1}
        self.channel_dict = {'layer1': 256, 'layer2': 512, 'layer3': 1024, 'layer4': 2048, 'avg_pool': 2048}
        self.kernel_size = {'layer1': 56, 'layer2': 28, 'layer3': 14, 'layer4': 7, 'avg_pool': 1}

        if 'c' in opt.resnet_path.split('/')[-1]:
            num_fc = num_fc_dic['cub']
        elif 'awa2' in opt.resnet_path.split('/')[-1]:
            num_fc = num_fc_dic['awa2']
        elif 'sun' in opt.resnet_path.split('/')[-1]:
            num_fc = num_fc_dic['sun']
        else:
            num_fc = 1000

        resnet.fc = nn.Linear(num_ftrs, num_fc)

        # 01 - load resnet to model1
        if opt.resnet_path != None:
            state_dict = torch.load(opt.resnet_path)
            resnet.load_state_dict(state_dict)
            print("resnet load state dict from {}".format(opt.resnet_path))

        modules = list(resnet.children())[:-1]
        self.resnet = nn.Sequential(*modules)
        self.fine_tune(True)

        # 02 - load cls weights
        # we left the entry for several layers, but here we only use layer4

        self.epsilon = 1e-4

        self.softmax = nn.Softmax(dim=1)
        self.softmax2d = nn.Softmax2d()
        self.sigmoid = nn.Sigmoid()

        self.softmax = nn.Softmax(dim=1)
        self.softmax2d = nn.Softmax2d()
        self.sigmoid = nn.Sigmoid()
        if opt.dataset == 'CUB':
            self.prototype_vectors = dict()
            for name in self.extract:
                prototype_shape = [312, self.channel_dict[name], 1, 1]
                self.prototype_vectors[name] = nn.Parameter(2e-4 * torch.rand(prototype_shape), requires_grad=True)
            self.prototype_vectors = nn.ParameterDict(self.prototype_vectors)
            self.ALE_vector = nn.Parameter(2e-4 * torch.rand([312, 2048, 1, 1]), requires_grad=True)
            self.macroclass_vector = nn.Parameter(2e-4 * torch.rand([312, 49]), requires_grad=True)

        elif opt.dataset == 'AWA2':
            self.prototype_vectors = dict()
            for name in self.extract:
                prototype_shape = [85, self.channel_dict[name], 1, 1]
                self.prototype_vectors[name] = nn.Parameter(2e-4 * torch.rand(prototype_shape), requires_grad=True)
            self.prototype_vectors = nn.ParameterDict(self.prototype_vectors)
            self.ALE_vector = nn.Parameter(2e-4 * torch.rand([85, 2048, 1, 1]), requires_grad=True)
            self.hasAttributeofclassLambda = ltn.Function(func=lambda x, y: torch.gather(x, 1, y).view(-1))
            self.macroclass_vector = nn.Parameter(2e-4 * torch.rand([85, 9]), requires_grad=True)
            self.attribute_vector = nn.Parameter(2e-4 * torch.rand([85, 85]), requires_grad=True)  # 原型参数
        elif opt.dataset == 'SUN':
            self.prototype_vectors = dict()
            for name in self.extract:
                prototype_shape = [102, self.channel_dict[name], 1, 1]
                self.prototype_vectors[name] = nn.Parameter(2e-4 * torch.rand(prototype_shape),
                                                            requires_grad=True)  # 原型参数
            self.prototype_vectors = nn.ParameterDict(self.prototype_vectors)
            self.ALE_vector = nn.Parameter(2e-4 * torch.rand([102, 2048, 1, 1]), requires_grad=True)  # 全局分支参数

        self.avg_pool = opt.avg_pool

        self.isofclass = ltn.Function(func=lambda x, y: torch.gather(x, 1, y).view(-1))
        self.hasAttribute = ltn.Function(func=lambda x, y: torch.gather(x, 1, y).view(-1))
        self.hasAttributeEnd = ltn.Function(func=lambda x, y: get_and_of_attributes(x, y))
        self.same_class = ltn.Function(func=lambda x, y: cosine_similarity(x, y, opt.c))
        self.hasAttributeEndError = ltn.Function(func=lambda x, y: get_and_of_attributes_error(x,
                                                                                               y))  # ltn.Predicate(model = Has_attribute())#[ltn.Predicate(model=Predicate_ltn(num_features=85,name=f.__str__()))  for f in range(85)]
        self.hasAttributeModel = Has_attribute()
        self.And = ltn.Connective(ltn.fuzzy_ops.AndProd())
        self.Exists = ltn.Quantifier(ltn.fuzzy_ops.AggregPMean(p=2), quantifier="e")
        self.Forall = ltn.Quantifier(ltn.fuzzy_ops.AggregPMeanError(p=2), quantifier="f")
        self.SatAgg = ltn.fuzzy_ops.SatAgg()
        self.Not = ltn.Connective(ltn.fuzzy_ops.NotStandard())
        self.Equiv = ltn.Connective(ltn.fuzzy_ops.Equiv(ltn.fuzzy_ops.AndProd(), ltn.fuzzy_ops.ImpliesReichenbach()))
        self.Implies = ltn.Connective(ltn.fuzzy_ops.ImpliesReichenbach())
        self.dist = lambda x, y: torch.unsqueeze(torch.norm(x - y, dim=1), dim=1)
        self.same_attribute = lambda x, y: torch.unsqueeze(same_attribute(x, y), dim=1)
        self.not_same_attribute = lambda x, y: torch.unsqueeze(not_same_attribute(x, y), dim=1)

    def forward(self, x, attribute, label=False, label_m=None, attribute_macroclass_seen=None, return_map=False,
                get_prediction=False, axioms_options=None, opt=None):
        """out: predict class, predict attributes, maps, out_feature"""
        # print('x.shape', x.shape)
        record_features = {}
        batch_size = x.size(0)

        x = self.resnet[0:5](x)  # layer 1
        record_features['layer1'] = x  # [64, 256, 56, 56]
        x = self.resnet[5](x)  # layer 2
        record_features['layer2'] = x  # [64, 512, 28, 28]
        x = self.resnet[6](x)  # layer 3
        record_features['layer3'] = x  # [64, 1024, 14, 14]
        x = self.resnet[7](x)  # layer 4
        record_features['layer4'] = x  # [64, 2048, 7, 7]
        pre_attri = dict()

        ######### 全局分支
        if self.avg_pool:
            pre_attri['final'] = F.avg_pool2d(F.conv2d(input=x, weight=self.ALE_vector), kernel_size=7).view(batch_size,
                                                                                                             -1)
        else:
            pre_attri['final'] = F.max_pool2d(F.conv2d(input=x, weight=self.ALE_vector), kernel_size=7).view(batch_size,
                                                                                                             -1)
            # 全局分支 [64, 312]

        output_final = self.softmax(pre_attri['final'].mm(attribute))
        images_x = ltn.Variable("images_x", output_final)
        images_x_features = ltn.Variable("images_x_features", pre_attri['final'])
        attribute_for_image = ltn.Variable("attribute_for_image", attribute.T[label])
        images_y_features = ltn.Variable("images_y_features", pre_attri['final'])
        if get_prediction == False:

            output_final_macroclass = self.softmax(pre_attri['final'].mm(self.macroclass_vector))  # [batchsize,类别数]
            x_global_macroclass = ltn.Variable("x_global_macroclass", output_final_macroclass)

            false_values_for_row = 15
            attribute = attribute.T
            false_indices = torch.randint(low=0, high=attribute.shape[1],
                                          size=(attribute.shape[0], false_values_for_row))

            mask = torch.zeros(attribute.shape[0], attribute.shape[1]).bool()
            mask[torch.arange(attribute.shape[0]).unsqueeze(1).type(torch.LongTensor), false_indices] = True
            mask = mask.cuda()
            attribute = torch.where(mask, 0, attribute)
            attribute = attribute.T

            output_final_masked = self.softmax(pre_attri['final'].mm(attribute))
            images_masked_x = ltn.Variable("images_masked_x", output_final_masked)
            label_x = ltn.Variable("label_x", label)
            label_y = ltn.Variable("label_y", label)
            label_classes = ltn.Variable("label_classes", torch.tensor(range(0, opt.seenclasses.shape[0])))

            label_x_m = ltn.Variable("label_x_m", label_m)

            my_dict = {

            }

            # globali

            sat_agg_class = self.Forall(
                ltn.diag(images_x, label_x),
                self.isofclass(images_x, label_x),
                p=axioms_options["p_axioms_class"]
            )
            my_dict["sat_agg_class"] = sat_agg_class

            sat_agg_same_attribute_class = self.Forall(
                ltn.diag(images_x_features, label_x),
                self.Forall(
                    ltn.diag(attribute_for_image, label_y),

                    self.same_class(images_x_features, attribute_for_image),
                    cond_vars=[label_x, label_y],
                    cond_fn=lambda x, y: torch.eq(x.value, y.value)),
                p=axioms_options["p_axioms_class"]
            )
            my_dict["sat_agg_same_attribute_class"] = sat_agg_same_attribute_class

            sat_agg_class_cluster_same_class = self.Forall(
                ltn.diag(images_x_features, label_x),
                self.Forall(
                    ltn.diag(images_y_features, label_y),

                    self.same_class(images_x_features, images_y_features),
                    cond_vars=[label_x, label_y],
                    cond_fn=lambda x, y: torch.eq(x.value, y.value),

                ),

                p=axioms_options["p_axioms_class_cluster"]
            )

            my_dict["sat_agg_class_cluster_same_class"] = sat_agg_class_cluster_same_class

            sat_agg_class_cluster_different_class = self.Forall(
                ltn.diag(images_x_features, label_x),
                self.Forall(
                    ltn.diag(images_y_features, label_y),

                    self.Not(self.same_class(images_x_features, images_y_features)),
                    cond_vars=[label_x, label_y],
                    cond_fn=lambda x, y: torch.not_equal(x.value, y.value),

                ),

                p=axioms_options["p_axioms_class_cluster"]
            )
            my_dict["sat_agg_class_cluster_different_class"] = sat_agg_class_cluster_different_class

            sat_agg_macroclass = self.Forall(
                ltn.diag(x_global_macroclass, images_x, label_x, label_x_m),
                self.Implies(self.isofclass(images_x, label_x), self.isofclass(x_global_macroclass, label_x_m)),
                p=axioms_options["p_axioms_macroclass"],
            )

            sat_agg_class_outlier_exists = self.Forall(
                label_classes,

                self.Exists(ltn.diag(images_masked_x, label_x), self.isofclass(images_masked_x, label_classes),
                            p=axioms_options["p_axioms_class_exists"],
                            cond_vars=[label_classes, label_x],
                            cond_fn=lambda x, y: torch.eq(x.value, y.value),

                            ))
            my_dict["sat_agg_class_outlier_exists"] = sat_agg_class_outlier_exists

            return 1 - self.SatAgg(sat_agg_class, sat_agg_class_outlier_exists,
                                   sat_agg_macroclass,
                                   sat_agg_class_cluster_same_class, sat_agg_class_cluster_different_class,
                                   sat_agg_same_attribute_class,
                                   p=axioms_options["p_all"]), my_dict

        else:
            attribute = ltn.Variable("attribute", attribute.T)
            predictions = images_x  # self.isofclass(images_x,attribute=attribute,get_prediction=True,size=attribute.value.shape[0])
            return predictions.value

    def fine_tune(self, fine_tune=True):
        """
        Allow or prevent the computation of gradients for convolutional blocks 2 through 4 of the encoder.

        :param fine_tune: Allow?
        """
        for p in self.resnet.parameters():
            p.requires_grad = False
        # If fine-tuning, only fine-tune convolutional blocks 2 through 4
        for c in list(self.resnet.children())[5:]:
            for p in c.parameters():
                p.requires_grad = fine_tune
