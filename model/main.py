from __future__ import print_function

import json
import os
import random
import sys
import time

import neptune
import numpy as np
import torch
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable
from tqdm import tqdm

import visual_utils
from main_utils import get_loader, Result
from main_utils import test_zsl, test_gzsl
from utils import init_log, print_and_log
from model_proto import resnet_proto_IoU
from neptune_logging import log_loss_training
from opt import get_opt
from visual_utils import prepare_attri_label

# from setproctitle import setproctitle
# setproctitle('wanggerong')


cudnn.benchmark = True

opt = get_opt()
# set random seed
if opt.manualSeed is None:
    opt.manualSeed = random.randint(1, 10000)
# print("Random Seed: ", opt.manualSeed)
random.seed(opt.manualSeed)
torch.manual_seed(opt.manualSeed)
np.random.seed(opt.manualSeed)
if opt.cuda:
    torch.cuda.manual_seed_all(opt.manualSeed)


def main():
    # load data
    data = visual_utils.DATA_LOADER(opt)
    print(opt)
    opt.test_seen_label = data.test_seen_label  # weird

    if opt.neptune_flag:

        opt.experiment = neptune.init(opt.project_name.__str__(), api_token=opt.token.__str__())

        # create experiment with defined parameters, uploaded source code and tags

        neptune.create_experiment(name=opt.experiment.__str__(), params=vars(opt))
        files = [os.path.join(dp, f) for dp, dn, filenames in os.walk(".") for f in filenames if
                 os.path.splitext(f)[1] == '.py']
        for f in files:
            neptune.log_artifact(f.replace("//", "/"))
        opt.neptune = neptune

        print("neptune finished")

    # define test_classes
    if opt.image_type == 'test_unseen_small_loc':
        test_loc = data.test_unseen_small_loc
        test_classes = data.unseenclasses
    elif opt.image_type == 'test_unseen_loc':
        test_loc = data.test_unseen_loc
        test_classes = data.unseenclasses
    elif opt.image_type == 'test_seen_loc':
        test_loc = data.test_seen_loc
        test_classes = data.seenclasses
    else:
        try:
            sys.exit(0)
        except:
            print("choose the image_type in ImageFileList")

    # prepare the attribute labels
    class_attribute = data.attribute
    print("attribute to cuda")
    attribute_zsl = prepare_attri_label(class_attribute, data.unseenclasses).cuda()
    attribute_seen = prepare_attri_label(class_attribute, data.seenclasses).cuda()
    attribute_macroclass_seen = prepare_attri_label(data.attribute_macroclass, data.seen_macroclass).cuda()
    attribute_gzsl = torch.transpose(class_attribute, 1, 0).cuda()

    print("init dataloader")
    # Dataloader for train, test, visual
    opt.unseenclasses = data.unseenclasses

    opt.seenclasses = data.seenclasses
    opt.total_classes = torch.sort(torch.cat((opt.seenclasses, opt.unseenclasses)))
    print("create dataloaders")
    trainloader, testloader_unseen, testloader_seen, visloader = get_loader(opt, data)
    print("train data loader initialized")

    # experiment name
    experiment_name = 'pretrainepoch{}_pretrainlr{}_classifierlr{}_{}ways-{}shots-{}_t{}'.format(opt.pretrain_epoch,
                                                                                             opt.pretrain_lr,
                                                                                             opt.classifier_lr,
                                                                                             opt.ways, opt.shots,
                                                                                             opt.contrative_loss_weight,
                                                                                             time.time())
    weight_name = 'pretrainepoch{}_pretrainlr{}_classifierlr{}_{}ways-{}shots-{}'.format(opt.pretrain_epoch,
                                                                                             opt.pretrain_lr,
                                                                                             opt.classifier_lr,
                                                                                             opt.ways, opt.shots,
                                                                                             opt.contrative_loss_weight)
    # visual_loss_path
    visual_loss_path = './out/visual/alpha/' + experiment_name + '/'

    print("logger ready")

    # store_result_path
    save_dir = './out/result/alpha/{}.txt'.format(experiment_name)
    init_log(save_dir)
    weight_path = opt.wpath + 'out/weight/alpha/' + weight_name + '/'
    if not os.path.exists(weight_path):
        os.makedirs(weight_path)

    # define attribute groups
    if opt.dataset == 'CUB':
        parts = ['head', 'belly', 'breast', 'belly', 'wing', 'tail', 'leg', 'others']
        group_dic = json.load(open(os.path.join(opt.root, 'data', opt.dataset, 'attri_groups_8.json')))
        sub_group_dic = json.load(open(os.path.join(opt.root, 'data', opt.dataset, 'attri_groups_8_layer.json')))
    elif opt.dataset == 'AWA2':
        parts = ['color', 'texture', 'shape', 'body_parts', 'behaviour', 'nutrition', 'activativity', 'habitat',
                 'character']
        group_dic = json.load(open(os.path.join(opt.root, 'data', opt.dataset, 'attri_groups_9.json')))
        sub_group_dic = {}

        opt.resnet_path = './pretrained_models/resnet101-5d3b4d8f.pth'
    elif opt.dataset == 'SUN':
        parts = ['functions', 'materials', 'surface_properties', 'spatial_envelope']
        group_dic = json.load(open(os.path.join(opt.root, 'data', opt.dataset, 'attri_groups_4.json')))
        sub_group_dic = {}

    # initialize model
    print('Create Model...')

    def create_model(attribute):

        model = resnet_proto_IoU(opt, attribute)

        return model

    model = create_model(attribute=data.attribute)

    if torch.cuda.is_available():
        model.cuda()

        attribute_zsl = attribute_zsl.cuda()
        attribute_seen = attribute_seen.cuda()
        attribute_macroclass_seen = attribute_macroclass_seen.cuda()
        attribute_gzsl = attribute_gzsl.cuda()

    layer_name = model.extract[0]  # only use one layer currently
    # compact loss configuration, define middle_graph
    # middle_graph = get_middle_graph(reg_weight[layer_name]['cpt'], model)

    # train and test

    result_gzsl_student = Result()
    result_zsl_student = Result()

    if opt.only_evaluate:
        print('Evaluate ...')

        model.load_state_dict(torch.load(opt.resume_path))
        print("eval mode from ", opt.resume_path)
        model.eval()
        # test zsl
        if not opt.gzsl:
            acc_ZSL = test_zsl(opt, model, testloader_unseen, attribute_zsl, data.unseenclasses)
            print_and_log('ZSL test accuracy is {:.1f}%'.format(acc_ZSL))
        else:
            # test gzsl
            acc_GZSL_unseen = test_gzsl(opt, model, testloader_unseen, attribute_gzsl, data.unseenclasses)
            acc_GZSL_seen = test_gzsl(opt, model, testloader_seen, attribute_gzsl, data.seenclasses)

            if (acc_GZSL_unseen + acc_GZSL_seen) == 0:
                acc_GZSL_H = 0
            else:
                acc_GZSL_H = 2 * acc_GZSL_unseen * acc_GZSL_seen / (
                        acc_GZSL_unseen + acc_GZSL_seen)

            print_and_log('GZSL test accuracy is Unseen: {:.1f} Seen: {:.1f} H:{:.1f}'.format(acc_GZSL_unseen, acc_GZSL_seen,
                                                                                      acc_GZSL_H))
    else:
        print('Train and test...')

        if opt.resume:
            print("Train and test... mode from ", opt.resume_path)
            model.load_state_dict(torch.load(opt.resume_path))

        global_step = 0
        for epoch in range(opt.nepoch):
            print("training")
            model.train()

            current_lr = opt.classifier_lr * (0.8 ** (epoch // 10))
            realtrain = epoch > opt.pretrain_epoch
            if not opt.resume and epoch < opt.pretrain_epoch:  # pretrain ALE for the first several epoches
                print_and_log('Pretraining with freezed layers')

                parameters = [model.ALE_vector, model.macroclass_vector]
                # , model.prototype_vectors[layer_name],model.macroclass_vector
                # parameters.extend(list(model.hasAttribute.parameters()))
                """
                for f  in model.hasAttributeModel.layers:
                    parameters.extend(f.parameters)
                """

                # parameters.extend(list(itertools.chain(*[list(f.parameters()) for f in model.isOfClassLTN.layers])))
                optimizer = optim.Adam(params=parameters, lr=opt.pretrain_lr, betas=(opt.beta1, 0.999))
            else:
                print('All layers training')
                parameters = [f for f in model.parameters()]
                # , model.prototype_vectors[layer_name],model.macroclass_vector
                # parameters.extend(list(model.hasAttribute.parameters()))
                """
                for f in model.hasAttributeModel.layers:
                    parameters.extend(f.parameters)
                """
                # Filter out layers that require_grad=false
                optimizer = optim.Adam(params=filter(lambda p: p.requires_grad, parameters),
                                       lr=current_lr, betas=(opt.beta1, 0.999))
                # optimizer = optim.Adam(params=filter(lambda p: p.requires_grad, model.parameters()),
                #         lr=opt.classifier_lr, betas=(opt.beta1, 0.999))

            loss_log = {'ave_loss': 0}

            batch = len(trainloader)  #### Set n_batch = 1000
            i = batch - 1
            if True:
                step = 0
                for i, (batch_input, batch_input2, batch_target, impath) in tqdm(enumerate(trainloader),
                                                                                 total=len(trainloader)):
                    step += 1
                    model.zero_grad()
                    # map target labels
                    if len(batch_target) == 2:
                        batch_target_macroclass = batch_target[1]
                        batch_target = batch_target[0]

                        batch_target_macroclass2 = visual_utils.map_label(batch_target_macroclass, data.seen_macroclass)

                    batch_target = visual_utils.map_label(batch_target, data.seenclasses)

                    input_v = Variable(batch_input)

                    label_v = Variable(batch_target)
                    label_m = Variable(batch_target_macroclass2)

                    if opt.cuda:
                        input_v = input_v.cuda()
                        label_v = label_v.cuda()
                        label_m = label_m.cuda()

                    axioms_options = {"p_axioms_class": 2.0, "p_axioms_macroclass": 1.0,
                                      "p_axioms_class_exists": 1.0, "p_axioms_class_cluster": 1.0, "p_all": 2.0}

                    if epoch >= 4:
                        axioms_options = {"p_axioms_class": 2.0, "p_axioms_macroclass": 2.0,
                                          "p_axioms_class_exists": 2.0, "p_axioms_class_cluster": 2.0, "p_all": 2.0}

                    if epoch >= 8:
                        if opt.axioms_exists:
                            axioms_options["p_axioms_class"] = 4.0
                            axioms_options["p_axioms_class_cluster"] = 4.0
                        if opt.weight_exists:
                            axioms_options["p_axioms_class_exists"] = 4.0
                        if opt.axioms_exists:
                            axioms_options["p_axioms_class"] = 4.0
                        if opt.macroclass_exists:
                            axioms_options["p_axioms_macroclass"] = 4.0
                        axioms_options["p_all"] = 4.0

                    if epoch >= 12:
                        if opt.axioms_exists:
                            axioms_options["p_axioms_class"] = 6.0
                            axioms_options["p_axioms_class_cluster"] = 6.0
                        if opt.weight_exists:
                            axioms_options["p_axioms_class_exists"] = 6.0
                        if opt.macroclass_exists:
                            axioms_options["p_axioms_macroclass"] = 6.0
                        axioms_options["p_all"] = 6.0

                    loss, list_axioms = model(input_v, attribute=attribute_seen, label=label_v,
                                              label_m=label_m, attribute_macroclass_seen=attribute_macroclass_seen,
                                              axioms_options=axioms_options, opt=opt)
                    if hasattr(torch.cuda, 'empty_cache'):
                        torch.cuda.empty_cache()

                    label_a = attribute_seen[:, label_v].t()
                    """
                    loss = Loss_fn(opt, loss_log, reg_weight, criterion, criterion_regre, model,
                                   output, pre_attri, attention, pre_class, label_a, label_v,
                                   realtrain, middle_graph, parts, group_dic, sub_group_dic)


                    loss_contrastive = criterion_contras(pre_attri[layer_name],label_v)
                    loss += loss_contrastive

                    if opt.consistency:
                        # consistency_weight = get_current_consistency_weight(opt, epoch)
                        # logger.scalar_summary('consistency_weight', consistency_weight, epoch)
                        # consistency_loss = consistency_weight * consistency_criterion(output, ema_logit) 

                        consistency_loss = opt.con * consistency_criterion(output, ema_logit) 

                    else:
                        consistency_loss = 0
                    loss += consistency_loss

                    """

                    loss_log['ave_loss'] += loss.item()
                    for f in list_axioms.keys():
                        if f in loss_log:
                            loss_log[f] += list_axioms[f].value
                        else:
                            loss_log[f] = list_axioms[f].value

                    """
                    loss_log['contrastive_loss'] += loss_contrastive.item()
                    loss_log['consistency_loss'] += consistency_loss.item()
                    """

                    loss.backward()
                    optimizer.step()
                    if opt.neptune_flag:
                        log_loss_training(opt, loss_log, step)

                    global_step += 1
                    # update_ema_variables(model, ema_model, opt.ema_decay, global_step)
                    # if step % 100 ==0:
                    #     print('\n[Epoch %d, Batch %5d] Train loss: %.3f '% (epoch+1, batch, loss_log['ave_loss'] / step))

            print_and_log('\nLoss log: {}'.format({key: (loss_log[key].item() if torch.is_tensor(loss_log[key]) else loss_log[key]) / batch for key in loss_log}))
            print_and_log('\n[Epoch %d, Batch %5d] Train loss: %.3f ' % (epoch + 1, batch, loss_log['ave_loss'] / batch))
            # logger.scalar_summary('Train loss', loss_log['ave_loss'] / batch, epoch+1)
            # logger.scalar_summary('Train contrastive loss', loss_log['contrastive_loss'] / batch, epoch+1)
            # logger.scalar_summary('Train consistency loss', loss_log['consistency_loss'] / batch, epoch+1)
            axioms_log = {'zsl': 0., 'top1_seen': 0., 'top1_unseen': 0., "H_gzsl": 0.}
            if (i + 1) == batch or (i + 1) % 200 == 0:
                ###### test #######
                print("testing")
                model.eval()

                # test zsl

                #### test zsl student
                student_acc_ZSL = test_zsl(opt, model, testloader_unseen, attribute_zsl, data.unseenclasses)
                # student_loss_ZSL, student_loss_contrastive_ZSL = test_gzsl_loss(opt, model, testloader_unseen, attribute_gzsl,reg_weight,criterion, criterion_regre,realtrain,middle_graph,parts,group_dic,sub_group_dic)

                # print('Test student ZSL loss: %.3f'% student_loss_ZSL)
                # logger.scalar_summary('Test ZSL loss', student_loss_ZSL, epoch+1)
                if student_acc_ZSL > result_zsl_student.best_acc:
                    # save model state
                    model_save_path = os.path.join(
                        weight_path + '{}__ZSL_id_{}.pth'.format(opt.dataset,
                                                                  opt.train_id))
                    torch.save(model.state_dict(), model_save_path)
                    print('model saved to:', model_save_path)
                result_zsl_student.update(epoch + 1, student_acc_ZSL)
                axioms_log["zsl"] += student_acc_ZSL
                print_and_log('[Epoch {}] ZSL test accuracy is {:.1f}%, Best_acc [{:.1f}% | Epoch-{}]'.format(epoch + 1,
                                                                                                        student_acc_ZSL,
                                                                                                        result_zsl_student.best_acc,
                                                                                                        result_zsl_student.best_iter))

                # test gzsl student model
                student_acc_GZSL_unseen = test_gzsl(opt, model, testloader_unseen, attribute_gzsl, data.unseenclasses)
                student_acc_GZSL_seen = test_gzsl(opt, model, testloader_seen, attribute_gzsl, data.seenclasses)
                axioms_log["top1_seen"] += student_acc_GZSL_seen
                axioms_log["top1_unseen"] += student_acc_GZSL_unseen

                if (student_acc_GZSL_unseen + student_acc_GZSL_seen) == 0:
                    student_acc_GZSL_H = 0
                else:
                    student_acc_GZSL_H = 2 * student_acc_GZSL_unseen * student_acc_GZSL_seen / (
                            student_acc_GZSL_unseen + student_acc_GZSL_seen)

                axioms_log["H_gzsl"] += student_acc_GZSL_H

                if opt.neptune_flag:
                    log_loss_training(opt, axioms_log, step)

                if student_acc_GZSL_H > result_gzsl_student.best_acc:
                    # save model state
                    # model_save_path = os.path.join('./out/{}_GZSL_id_{}.pth'.format(opt.dataset, opt.train_id))
                    model_save_path = os.path.join(
                        weight_path + '{}_student_GZSL_id_{}.pth'.format(
                            opt.dataset, opt.train_id))
                    torch.save(model.state_dict(), model_save_path)
                    print('model saved to:', model_save_path)

                result_gzsl_student.update_gzsl(epoch + 1, student_acc_GZSL_unseen, student_acc_GZSL_seen,
                                                student_acc_GZSL_H)

                print_and_log('\n[Epoch {}] GZSL test student accuracy is Unseen: {:.1f} Seen: {:.1f} H:{:.1f}'
                      '\n           Best_H student [Unseen: {:.1f}% Seen: {:.1f}% H: {:.1f}% | Epoch-{}]'.
                      format(epoch + 1, student_acc_GZSL_unseen, student_acc_GZSL_seen, student_acc_GZSL_H,
                             result_gzsl_student.best_acc_U, result_gzsl_student.best_acc_S,
                             result_gzsl_student.best_acc, result_gzsl_student.best_iter))

                # storage.txt
                print('--' * 60)  # Print 60 '--'
                print('student')
                print_and_log('epoch:{} - Train loss: {:.3f}    '.format(epoch + 1, loss_log['ave_loss'] / batch))
                print_and_log(
                    'epoch:{} - student_acc_seen: {:.1f}          student_acc_novel: {:.1f}            student_H: {:.1f}'.format(
                        epoch + 1, student_acc_GZSL_seen, student_acc_GZSL_unseen, student_acc_GZSL_H))
                print('epoch:{} - Test student ZSL accuracy: {:.3f}   '.format(epoch + 1, student_acc_ZSL))
        print_and_log("\n-----------FINISHED-----------\n")
        print_and_log('Best_student_H [Unseen: {:.1f}% Seen: {:.1f}% H: {:.1f}% | Epoch-{}]'.format(
            result_gzsl_student.best_acc_U, result_gzsl_student.best_acc_S,
            result_gzsl_student.best_acc, result_gzsl_student.best_iter))
        print_and_log('Best_student__T1 [T1: {:.1f}%  | Epoch-{}]'.format(result_zsl_student.best_acc,
                                                                    result_zsl_student.best_iter))


if __name__ == '__main__':
    main()
