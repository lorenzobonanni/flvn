
The current project page provides the Pytorch implementation for the paper:
# Fuzzy Logic Visual Network (FLVN): A neuro-symbolic approach for visual features matching
## Authors: Francesco Manigrasso, Lia Morra, Fabrizio Lamberti
## Abstract:
From the very introduction of the zero-shot learning topic, visual attributes have been shown to play an important role. In order to better transfer attribute-based knowledge from known to unknown classes, we argue that an image representation with integrated attribute localization ability would be beneficial for zero-shot learning. To this end, we propose a novel zero-shot representation learning framework that jointly learns discriminative global and local features using only class-level attributes. While a visual-semantic embedding layer learns global features, local features are learned through an attribute prototype network that simultaneously regresses and decorrelates attributes from intermediate features. We show that our locality augmented image representations achieve a new state-of-the-art on three zero-shot learning benchmarks. As an additional benefit, our model points to the visual evidence of the attributes in an image, e.g. for the CUB dataset, confirming the improved attribute localization ability of our image representation.

## Link
The preprint is available for download at https://arxiv.org/pdf/2307.16019.pdf


## Implementation notes 

- Logic Tensor Networks are a neural symbolic framework originally developed by Luciano Serafini and Artur d'Avila Garcez Artur
- The Torch implementation of the framework is cloned from https://github.com/tommasocarraro/LTNtorch.git
- Datasets are cloned from [CUB](http://www.vision.caltech.edu/datasets/cub_200_2011/),[AWA2](https://cvml.ista.ac.at/AwA2/),[SUN](https://groups.csail.mit.edu/vision/SUN/hierarchy.html),
- Additional information about the datasets cloned from [APN-DATA](https://drive.google.com/file/d/1bCZ28zJZNzsRjlHxH_vh2-9d7Ln1GgjE/view)
- Download the repository with, for example, `git clone https://gitlab.com/grains2/flvn.git`.


## Data
- `AWA2` is the dataset from *Zero-Shot Learning -- A Comprehensive Evaluation of the Good, the Bad and the Ugly* by Yongqin Xian, Christoph H. Lampert, Bernt Schiele, Zeynep Akata
- `CUB` is the dataset from *Caltech-UCSD Birds-200-2011 (CUB-200-2011)* by Catherine Wah, Steve Branson, Peter Welinder, Pietro Perona, Serge Belongie
- `SUN` is the dataset from *SUN Database: Large-scale Scene Recognition from Abbey to Zoo* by Jianxiong Xiao; James Hays; Krista A. Ehinger; Aude Oliva; Antonio Torralba

- Semantic relations from Wordnet
```sh
$ python model/wordnet.py
$ python model/wordnet_cub.py
```

## Training and Evaluation  
- All weights of pretrained models are downloaded from https://drive.google.com/file/d/1c5scuU0kZS5a9Rz3kf5T0UweCvOpGsh2/view


- To reproduce the results on AWA2, use the following command
```sh
python main.py python ./model/main.py --dataset AWA2 --cuda --nepoch 30 --pretrain_epoch 2 --pretrain_lr 1e-4 --classifier_lr 1e-7 --manualSeed 8275 --regular 0.0005 --l_regular 0.5e-6 --cpt 2e-9 --avg_pool --batch_size 128 --calibrated_stacking 0.7 --all --gzsl  --c 0.01 --train_mode distributed  --train_mode distributed  --ways 8  --shots 12
```
- To reproduce the results on CUB, use the following command
```sh
python main.py python ./model/main.py --dataset CUB --calibrated_stacking 0.7 --cuda --nepoch 30 --batch_size 128 --train_id 0 --manualSeed 3131 --pretrain_epoch 5 --pretrain_lr 1e-4 --classifier_lr 1e-6  --regular 5e-5   --l_regular 4e-2 --cpt 1e-9 --use_group --gzsl --c 1 --train_mode distributed --train_mode distributed  --ways 8  --shots 12
```

- To reproduce the results on SUN, use the following command
```sh
python main.py python ./model/main.py --calibrated_stacking 0.4 --dataset SUN --cuda --nepoch 30 --pretrain_epoch 4 --pretrain_lr 1e-3 --classifier_lr 1e-6 --manualSeed 2347 --batch_size 128  --regular 1e-3 - --l_regular 5e-3  --avg_pool --use_group --cpt 2e-7 --gzsl   --c 1 --train_mode distributed --train_mode distributed  --ways 8  --shots 12
```

# Citation

If you make use of the code in your research, please cite our paper:

@article{manigrasso2023fuzzy,
  title={Fuzzy Logic Visual Network (FLVN): A neuro-symbolic approach for visual features matching},
  author={Manigrasso, Francesco and Morra, Lia and Lamberti, Fabrizio and others},
  journal={LECTURE NOTES IN COMPUTER SCIENCE},
  year={2023},
  publisher={Springer}
}


# Contributors & Maintainers
Francesco Manigrasso, Lia Morra and Fabrizio Lamberti
For inquiries and information: name.surname @ polito.it 
